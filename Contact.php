<?php
   require 'PHPMailer/PHPMailerAutoload.php';


?>

<!DOCTYPE html>
<html lang="en">
<head>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Lora:400i" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection"/>
     <link href="https://fonts.googleapis.com/css?family=Tangerine" rel="stylesheet">
      <!--use own styling-->
      <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Tangerine:700" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <!--Let browser know website is optimized for mobile-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Shrashta Photography</title>
    <style>
        ::placeholder { 
             color: black;
            opacity: 1; 
            font-family:'Cormorant Garamond', serif;
            font-size: larger; 
        }
        .custom-form{
            color:white;
            height:40px !important;
            border:2px solid white !important;
            border-radius:10px !important;
        }
        .custom-form-message{
            color:white;
            height:70px !important;
            border:2px solid white !important;
            border-radius:10px !important;
        }
        .label{
            color:white;
            font-family:'Titillium web', sans-serif;
            
        }
       
        #button{
          color:#ef486f;
         
          font-size:20px;
         
        }
       
    </style>
</head>
<body >
        
    <!--_________________navbar for pc_____________________-->
    <?php 
        include('navbar.php');
    ?>

  <!--____________________________________mobile________________________________________________________________________________________-->
  <div class="row show-on-medium-and-down hide-on-large-only" style="margin:0px;">
          
          <!--logo for mobile-->
          <!-- <div class="logo center white show-on-medium-and-down hide-on-large-only" id="shrastalogo">
             <img  class="responsive-img" src="images/mainlogo1.png" style="width:150px;height:70px;">
          </div>  -->
         <!--navbar for mobile-->
         <div class="col s12 black-text white" id="navbar-mobile" style="">
             <ul class="collapsible">
                 <li>
                   <div class="collapsible-header white" style="font-size:20px;"><i class="material-icons" style="position:relative;top:5px;">menu</i>Menu
                   <div class="logo center white show-on-medium-and-down hide-on-large-only" id="shrastalogo" style="position:absolute; right:40px">
                     <img  class="responsive-img" src="images/Shrashta Photography black Logo.png" style="width:70px;height:35px;">
                    </div>   
                  </div>
                   <div class="collapsible-body"><a href="index.php" id="button"><div style="width:100%">HOME</div></a></div>
                   <div class="collapsible-body"><a href="admin/index.php" id="button"><div style="width:100%">E-ALBUM</div></a></div>
                    <div class="collapsible-body"> <a href="services.php" id="button"><div style="width:100%">SERVICES</div></a></div>
                   <div class="collapsible-body"><a href="about us.php" id="button"><div style="width:100%">ABOUT US</div></a></div>
                   <div class="collapsible-body"><a href="Contact.php" id="button"><div style="width:100%">CONTACT</div></a></div>
                 </li>
              </ul>
           </div>

       </div>
   <!--_______________________________________end of mobile header________________________________________-->

    <!--___________________________________________body_____________________________-->
    <div class="" style="background-image:url('./img/Image.jpg');background-size:cover; margin:0px;">
            <div class="row ">   
                    <div class="container">  
                        <div class="col s12 m12 l12" align="center" style="padding-top:30px;">
                        
                        <h2 class="center" style="font-family: 'Tangerine', cursive ;font-weight:700; color:white;">Stay Connected</h2>
                        <img src="images/White Heart Shape.png" class="responsive-img "style="width:250px;">
                        <p class="center" style="font-family: 'Titillium Web', sans-serif;font-size:20px;font-weight:400;color:white;">
                            Let's begin with a hello
                            <br>"It would be an absolute honour to capture your love,&nbsp;growth and those real moments".</p>
                            <p class="center" style="font-family: 'Titillium Web', sans-serif;font-size:20px;font-weight:400;color:white;">
                                For availability,&nbsp;pricing or any other queries&nbsp;-&nbsp;please provide details and dates to get prompt reply.</p>
                    </div>
                    </div>
                    </div>
                    <!-- form start -->

                   <div class="container" style="">
                       <div class="row">
                       <div class="col s12 m6 offset-m3">
                       <form method="post" action="contact_us.php" enctype="multipart/form-data">
                               <div class="row">
                                 <div class="col s12 m12 l12" >
                                     <label for="name" class="label">Name</label>
                                     <input type="text" name="name" class="custom-form" id="name">
                                 </div>
                                 <div class="col s12 m6 l6">
                                      <label for="email" class="label">Email</label>
                                      <input type="email" name="email" class="custom-form" id="email">
                                 </div>
                                 <div class="col s12 m6 l6" >
                                        <label for="phone" class="label">Phone No</label>
                                         <input type="number" name="phone" class="custom-form" id="phone" >
                                 </div>
                                 <div class="col s12 m12 l12"  >
                                       <label for="message" class="label">Message</label>
                                        <textarea col="12" row="10" name="message" class="custom-form-message"  id="message"></textarea>
                                 </div>
                                                   
                                 <div class="button center" style=" ">
                                    <button class="btn waves-effect waves-light" style=" background-color:#ef486f;border-radius:10px;margin-top:30px; " type="submit" name="submit">Send </button>
                               </div>
                              </div>
                           </form>
                        </div>
                    </div>         
                  </div>
                  <!-- form end -->
                  <div class="row" style="margin:0px;">
                        <div class="container ">
                        <div class="row hide-on-small-only">
                        <div class="col  m6 l6 center" style="padding-left:140px;" >
                            <a class="btn-floating btn-small waves-effect waves-light"><i class="material-icons small" style="background-color:white;color:black;">local_phone</i></a><span style="font-size:15px;font-family:'Titillium web', sans-serif;color:white;margin-left:10px;"> +91-9583333562 , +91-9040250369</span>
                            </div>
                    
                            <div class="col m6 l6 center" style="padding-right:100px;" >
                            <a class="btn-floating btn-small waves-effect waves-light"><i class="material-icons small" style="background-color:white;color:black;">email</i></a><span style="font-size:15px;font-family:'Titillium web', sans-serif;color:white;margin-left:10px;"> shrashta.creation@gmail.com</span>
                           </div>
                           <div  class="col m12 l12 center"  style="padding-top:20px;">
                            <a class="btn-floating btn-small waves-effect waves-light"><i class="material-icons small" style="background-color:white;color:black;">location_on</i></a><span style="font-size:15px;font-family:'Titillium web', sans-serif;color:white;margin-left:10px;"> Deulasahi,&nbsp;Baripada,&nbsp;Odisha.</span> 
                            </div>
                            
                        </div>    
                     </div>
                    </div>
                      
                <!-- for mobile -->
                      <div class="row" style="margin:0px;">
                        <div class="container ">
                        <div class="row hide-on-med-and-up">
                        <div class="col s12   " style="padding-right:80px;"  >
                           <div class="col s4  "  style="padding-left:40px;"> <a class="btn-floating btn-small waves-effect waves-light;"><i class="material-icons small" style="background-color:white;color:black;">local_phone</i></a></div>
                          <div class="col s8  "  style="margin-top:-5px;"> <span style="font-size:15px;font-family:'Titillium web', sans-serif;color:white;"> +91-9583333562 <br>  +91-9040250369</span></div>
                            </div>
                    
                            <div class="col s12 center " style="padding-top:20px;">
                            <a class="btn-floating btn-small waves-effect waves-light"><i class="material-icons small" style="background-color:white;color:black;">email</i></a><span style="font-size:15px;font-family:'Titillium web', sans-serif;color:white;padding-left:10px;"> shrashta.creation@gmail.com</span>
                           </div>
                           <div  class="col s12  center "style="padding-top:20px;padding-right:20px;" >
                            <a class="btn-floating btn-small waves-effect waves-light"><i class="material-icons small" style="background-color:white;color:black;">location_on</i></a><span style="font-size:15px;font-family:'Titillium web', sans-serif;color:white;padding-left:10px;"> Deulasahi,&nbsp;Baripada,&nbsp;Odisha.</span> 
                            </div>
                            
                        </div>    
                     </div>
                    </div>



            <footer class="page-footer black hide-on-small-only">
                <div class="container">
                    <div class="row" style="margin-bottom:0px;">
                    <div class="col l12 m12 ">
                        <div id="footer-icons">
                            <h6 class="center-align white-text"><span style="position:relative;top:5px;">Tel&nbsp;:</span> <span>&nbsp;+91-9583333562 <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  +91-9040250369</span></h6>
                            <a class="waves-effect waves-light red btn-floating social google"href="https://plus.google.com/115064191067160860506"><i class="fa fa-google"></i> Sign in with Google</a>
                                <a class="waves-effect waves-light blue btn-floating social google"  href=""><i class="fa fa-twitter"></i> Sign in with Google</a>
                                <a class="waves-effect waves-light red accent-4 btn-floating social google" href="https://www.youtube.com/channel/UCvmkIuaSUbMIR3W3zZ1Edpw/videos"><i class="fa fa-youtube"></i> Sign in with Google</a>
                                <a class="waves-effect waves-light  btn-floating indigo  social google" style="background-color:#3a559f;" href="https://www.facebook.com/ShrashtaCreation/?ref=br_rs"><i class="fa fa-facebook"></i> Sign in with Google</a>
                                <h6 class="center-align" style=" font-family: 'Titillium Web', sans-serif;font-size:15px;font-weight:400;color:white;padding-bottom:20px;">© 2017-2018 All Rights Reserved<br>Designed By<a href="http://appstone.in" style="color:#ef789b;"> Appstone</a></h6>
                        </div>
                    </div>
                    </div>
                </div>
            </footer> 

    </div>
         <footer class="page-footer black hide-on-med-and-up" style="margin:0px;">
                <div class="container"  >
                    <div class="row" style="margin-bottom:0px;">
                    <div class="col s12">
                        <div id="footer-icons">
                            <h6 class="center-align white-text"><span style="position:relative;top:5px;"><span style="position:relative;top:5px;">Tel&nbsp;:</span> <span>&nbsp;+91-9583333562 <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  +91-9040250369</span></h6>
                            <a class="waves-effect waves-light red btn-floating social google"href="https://plus.google.com/115064191067160860506"><i class="fa fa-google"></i> Sign in with Google</a>
                                <a class="waves-effect waves-light blue btn-floating social google"  href=""><i class="fa fa-twitter"></i> Sign in with Google</a>
                                <a class="waves-effect waves-light red accent-4 btn-floating social google" href="https://www.youtube.com/channel/UCvmkIuaSUbMIR3W3zZ1Edpw/videos"><i class="fa fa-youtube"></i> Sign in with Google</a>
                                <a class="waves-effect waves-light  btn-floating indigo  social google" style="background-color:#3a559f;" href="https://www.facebook.com/ShrashtaCreation/?ref=br_rs"><i class="fa fa-facebook"></i> Sign in with Google</a>
                                <h6 class="center-align" style=" font-family: 'Titillium Web', sans-serif;font-size:15px;font-weight:400;color:white;padding-bottom:20px;">© 2017-2018 All Rights Reserved<br>Designed By<a href="http://appstone.in" style="color:#ef789b;"> Appstone</a></h6>
                        </div>
                    </div>
                    </div>
                </div>
            </footer> 

    </div>
  <!-- end mob view -->                  
           
                         
                             
                

                   

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    
    <script type="text/javascript" src="js/materialize.min.js"></script>
   
   <script>
      $(document).ready(function() {
        $('.parallax').parallax();
      $('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 15, // Creates a dropdown of 15 years to control year,
    today: 'Today',
    clear: 'Clear',
    close: 'Ok',
    closeOnSelect: false, // Close upon selecting a date,
    container: undefined, // ex. 'body' will append picker to body
  })
  $('.dropdown-button').dropdown({hover: true,belowOrigin:false});
});
   </script>

 <?php
      
      if(isset($_POST['submit'])) {
        $content='
        <body style="border:1px solid black;">
            <div align="center" style=" background:linear-gradient(#000000,#181818,#000000);margin:auto;width:600px" >
           <img src="https://s15.postimg.cc/gglti88cr/mainlogo1.png" style="width:200px;">
              
            </div>
            <br>
            <div style="width:400px;height:300px;margin:auto;">
                    <span style="padding-left:10px;"><b>Name:</b>'.$_POST['name'].'</span> <br><br>
                    <span style="padding-left:10px;"><b>Email:</b>'.$_POST['email'].'</span> <br><br>
                    <span style="padding-left:10px;"><b>Phone No:</b>'.$_POST['phone'].'</span><br><br> 
                    
                    <span style="padding-left:10px;"><b>Message:</b>'.$_POST['message'].'</span> <br><br> 
            </div>
        </body>
        </html>
        ';
        $mail = new PHPMailer;
        $mail->isSendmail();
        //Set who the message is to be sent from
        $mail->setFrom("pragyandas242@gmail.com","Shrashta Photography");
        //Set an alternative reply-to address
        $mail->addReplyTo($_POST['email'], $_POST['name']);
        //Set who the message is to be sent to
        $mail->addAddress('pragyandas242@gmail.com',"Shrashta Photography"); 
        //Set the subject line
        $mail->Subject = 'From Website Contact Form';
        //Read an HTML message body from an external file, convert referenced images to embedded,
        //convert HTML into a basic plain-text alternative body
        //Replace the plain text body with one created manually
        $mail->Body    = $content;
        $mail->isHTML();
        $mail->AltBody = "";
        
        if (!$mail->send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
            echo "
            <script>
            Materialize.toast('Message could not be sent', 4000);
            <script>
            ";
        } else {
          echo "
          <script>
          Materialize.toast('Message sent successfully', 4000);
          <script>
          ";
        }
      }
      ?>


   </body>
</html>