<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
<link href="https://fonts.googleapis.com/css?family=Tangerine:400,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lora:400i" rel="stylesheet"> 
 
<link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection"/>
        <!--use own styling-->
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <!--Let browser know website is optimized for mobile-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Shrashta Photography</title>
    <style>
          #button{
          color:#ef486f;
          
          font-size:20px;
         

        }
        </style>
</head>
<body> 
   
    <!--navbar-->
        <?php 
            include('navbar.php');
        ?>
        
        <!--____________________________________mobile________________________________________________________________________________________-->
        <div class="row show-on-medium-and-down hide-on-large-only" style="margin:0px;">
          
          <!--logo for mobile-->
          <!-- <div class="logo center white show-on-medium-and-down hide-on-large-only" id="shrastalogo">
             <img  class="responsive-img" src="images/mainlogo1.png" style="width:150px;height:70px;">
          </div>  -->
         <!--navbar for mobile-->
         <div class="col s12 black-text white" id="navbar-mobile" style="">
             <ul class="collapsible">
                 <li>
                   <div class="collapsible-header white" style="font-size:20px;"><i class="material-icons" style="position:relative;top:5px;">menu</i>Menu
                   <div class="logo center white show-on-medium-and-down hide-on-large-only" id="shrastalogo" style="position:absolute; right:40px">
                     <img  class="responsive-img" src="images/Shrashta Photography black Logo.png" style="width:70px;height:35px;">
                    </div>   
                  </div>
                   <div class="collapsible-body"><a href="index.php" id="button"><div style="width:100%">HOME</div></a></div>
                   <div class="collapsible-body"><a href="admin/index.php" id="button"><div style="width:100%">E-ALBUM</div></a></div>
                    <div class="collapsible-body"> <a href="services.php" id="button"><div style="width:100%">SERVICES</div></a></div>
                   <div class="collapsible-body"><a href="about us.php" id="button"><div style="width:100%">ABOUT US</div></a></div>
                   <div class="collapsible-body"><a href="Contact.php" id="button"><div style="width:100%">CONTACT</div></a></div>
                 </li>
              </ul>
           </div>

       </div>
   <!--_______________________________________end of mobile header________________________________________-->


              <!--body for pc-->
             <div style="background-image:url('./images/aboutusbg.jpg');background-size:cover;background-attachment: fixed;">
              <div class="row">   
              <div class="container">  
                <div class="col m12 l12" align="center" style="padding-top:20px;">
                
                <h2 class="center" style="font-family: 'Tangerine', cursive ;font-weight:700; color:white;">About Us</h2>
                <img src="images/White Heart Shape.png" class="responsive-img "style="width:250px;">
                <p class="center" style="font-family: 'Titillium Web', sans-serif;font-size:20px;font-weight:400;color:white;">
                    Planning your dream wedding?
                     <br> We are here to capture your dreams and let the magic happen. 
                      So that when you look back at those photographs you would feel special..</p>
               </div>
               </div>
            </div>    

            <div class="row">   
                <div class="container">                 
                 <div class="col m12 l12" align="center" style="padding-top:5px;">
                    <h2 class="center" style="font-family: 'Tangerine', cursive ;font-weight:700; color:white;padding:0px;margin:0px;">Who we are?</h2> <br>
                    <img src="images/White Heart Shape.png" class="responsive-img " style="width:250px;">
                    <p class="center" style="font-family: 'Titillium Web', sans-serif;font-size:20px;font-weight:400;color:white;">
                     We are a team of professional photographers and cinematographers dedicated to capturing the best moments of your event.
                     We make sure that our clicks will leave an everlasting impression within you. So that when  you look back into those photographs 
                     it wil take you back into those moments making you feel special.</p> <br>
                     </div>
                 </div>          
                </div>
                <div class="row" style="margin:0px;padding-bottom:30px;" >   
                    <div class="container">        
                    <div class="col m12 l12" align="center" style="padding-top:-5px;">
                    <h2 class="center" style="font-family: 'Tangerine', cursive ;font-weight:700; color:white;margin:0px;">What we do?</h2> <br>
                    <img src="images/White Heart Shape.png" class="responsive-img " style="width:250px;">
                    <p class="center" style="font-family: 'Titillium Web', sans-serif;font-size:20px;font-weight:400;color:white;">
                        We have been in business since 2011 and served over 100 clients in Odisha. 

                            The best part about photography is that it lets you relive your best moments time and time 
                            again and this is what drives our passion towards photography. Be it any kind of event, our entire 
                            team will be there to give you an experience that you will never forget. 
                            
                            The only thing that justifies the work of a photographer is his work. So please see some of our work 
                            under our gallery section and decide for yourself if we can do the job for you.
                            
                            First time in Odisha, you can view a preview of your photographs on our E-album.  </p>
                            
                         </div>        
                      </div>
                     </div>
                    
                
                <footer>
                <div class=" hide-on-small-only" style="margin:0px;"> 
                        <div class="col m12 l12 black center-align white-text" style="padding-bottom:0;position:relative;top:20%;">
                                <h6 class="center-align white-text" style="padding-top:20px;margin:0px;"><span style="position:relative;top:5px;">Tel&nbsp;:</span> <span>&nbsp;+91-9583333562 <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  +91-9040250369</span></h6>
                                <div id="footer-icons" style="margin-bottom:0;padding-top:10px;">
                                <a class="waves-effect waves-light red btn-floating social google"href="https://plus.google.com/115064191067160860506"><i class="fa fa-google"></i> Sign in with Google</a>
                                <a class="waves-effect waves-light blue btn-floating social google" href=""><i class="fa fa-twitter"></i> Sign in with Google</a>
                                <a class="waves-effect waves-light red accent-4 btn-floating social google" href="https://www.youtube.com/channel/UCvmkIuaSUbMIR3W3zZ1Edpw/videos"><i class="fa fa-youtube"></i> Sign in with Google</a>
                                <a class="waves-effect waves-light  btn-floating  social google" style="background-color:#3a559f;" href="https://www.facebook.com/ShrashtaCreation/?ref=br_rs"><i class="fa fa-facebook" ></i> Sign in with Google</a>
                               </div>
                               <h6 class="center-align" style=" font-family: 'Titillium Web', sans-serif;font-size:15px;font-weight:400;color:white;padding-bottom:20px;margin:0px;padding-top:10px;">© 2017-2018 All Rights Reserved<br>Designed By<a href="http://appstone.in" style="color:#ef789b;"> Appstone</a></h6>
                              </div>
                              </div>
                      </footer> 
        </div> 

       <footer style="margin:0px;">
       <div class=" show-on-small hide-on-med-and-up" style="margin:0px;">
                        <div class="col s12 m12 l12 black center-align white-text" style="padding-bottom:0;position:relative;top:20%;">
                                <h6 class="center-align white-text" style="padding-top:20px;margin:0px;"><span style="position:relative;top:5px;">Tel&nbsp;:</span> <span>&nbsp;+91-9583333562 <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  +91-9040250369</span></h6>
                                <div id="footer-icons" style="margin-bottom:0;padding-top:10px;">
                                <a class="waves-effect waves-light red btn-floating social google"href="https://plus.google.com/115064191067160860506"><i class="fa fa-google"></i> Sign in with Google</a>
                                <a class="waves-effect waves-light blue btn-floating social google" href=""><i class="fa fa-twitter"></i> Sign in with Google</a>
                                <a class="waves-effect waves-light red accent-4 btn-floating social google" href="https://www.youtube.com/channel/UCvmkIuaSUbMIR3W3zZ1Edpw/videos"><i class="fa fa-youtube"></i> Sign in with Google</a>
                                <a class="waves-effect waves-light  btn-floating  social google" style="background-color:#3a559f;" href="https://www.facebook.com/ShrashtaCreation/?ref=br_rs"><i class="fa fa-facebook" ></i> Sign in with Google</a>
                               </div>
                               <p class="center-align" style=" font-family: 'Titillium Web', sans-serif;font-size:15px;font-weight:400;color:white;margin:0px;padding-bottom:20px;padding-top:10px;">© 2017-2018 All Rights Reserved<br>Designed By<a href="http://appstone.in" style="color:#ef789b;"> Appstone</a></p>
                              </div>
                              </div>
                      </footer>      
            </div>
     <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script>
    $(document).ready(function(){
    $(".dropdown-button").dropdown({
          hover: true,
        belowOrigin: false
        }); 
    });
    </script>
</body>
</html>