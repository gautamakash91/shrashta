<?php
include_once("../db.php");
 
    session_start();
    $_SESSION['admin'];
    if ($_SESSION['admin'] == "") {
        header('location:index.php');
    }

?>
    
    <!DOCTYPE html>
    <html>
        <head>
       <?php include_once("../header.php");
            ?>
        
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        </head>
        
        <body>
        
        <?php include_once("navbar.php");
               ?>             

 
<!--________________________________________Body part_____________________________-->
<?php
    $qry = $conn->query("SELECT * FROM album");
    $row = $qry->num_rows;

    $sql = $conn->query("SELECT * FROM admin_login WHERE type='admin'");
    $val = $sql->num_rows;

    $result = $conn->query("SELECT * FROM images_album");
    $res = $result->num_rows;
  ?>                    
        <section class="col s12 m12 l12  " >
                 <div class="center">
                    
                      <a href="#" class=" brand-logo"><h3>Dashboard</h3></a>
                </div>
                <div class="divider"></div>


                <div class="row">
               
                        <div class="col s12 m4">
                        <div class="card-panel"  >
                            <span class="grey-text"><h5>NO OF ALBUM</h5><br/>
                           <h3 class="blue-text"><b><?php echo $row;?></b></h3>
                            </span><br/><br/><br/>
                            
                                </div>
                                </div>
                                
                                <div class="col s12 m4">
                        <div class="card-panel ">
                            <span class="grey-text"><h5>NO OF IMAGES</h5><br/>
                           <h3 class="green-text"><b> <?php echo $val;?></b></h3>
                            </span><br/><br/><br/>
                            
                           
                                </div>
                                </div>
                    
                                <div class="col s12 m4">
                        <div class="card-panel ">
                            <span class="grey-text"><h5>NO OF ADMIN</h5><br/>
                           <h3 class="red-text"><b> <?php echo $res;?></b></h3>
                            </span><br/><br/><br/>
                            
                            
                                </div>
                                </div>
                    
                    
       </section>
                     
     </div>
                     
        </body>
    </html>
            