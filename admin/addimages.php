<?php

//session_start(); 

session_start();
$_SESSION['admin'];
if ($_SESSION['admin'] == "") {
    header('location:index.php');
}
?>


<html>
  <head>
      <?php 
      include('../header.php');
      ?>
  </head>

  <body>
    
   <div class="container " style="border: 2px solid #26a69a;border-radius: 5px;padding: 2%;">
   <center><h5 style="color:#26a69a;">Image Upload</h5></center>

     
       <div class="file-field input-field">
      <div class="btn">
        <span>Image Upload</span>
        <input type="file" name="files[]" id="files" multiple>
      </div>
      <div class="file-path-wrapper">
        <input class="file-path validate" type="text" placeholder="Upload one or more files">
      </div>
    </div>
  
  <center><button class="btn"  name = "button" onclick="addfile()">SUBMIT</button></center>

  </div>
  <script src="https://www.gstatic.com/firebasejs/4.13.0/firebase.js"></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyAut3gt9eI6j6q_0Mzt5e9vfvxKCKGfND8",
    authDomain: "wedding-e5b24.firebaseapp.com",
    databaseURL: "https://wedding-e5b24.firebaseio.com",
    projectId: "wedding-e5b24",
    storageBucket: "gs://wedding-e5b24.appspot.com/",
    messagingSenderId: "887353312379"
  };
  firebase.initializeApp(config);

            // Get a reference to the storage service, which is used to create references in your storage bucket
            var storage = firebase.storage();

            // Create a storage reference from our storage service
            var storageRef = storage.ref();
            

        function addfile(){
            console.log("upload initiated");
            var metadata = {
                contentType: 'image/jpg'
            };

            var i;
            var imgs = $("#files")[0].files;
            console.log("length: "+imgs.length);
            for (i = 0; i <  imgs.length; i++) {
             
            var file = $("#files")[0].files[i];

            
           
            var uploadTask = storageRef.child(file.name).put(file, metadata);

            // Listen for state changes, errors, and completion of the upload.
        uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
        

        
        function(snapshot) {
            // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            console.log('Upload is ' + progress + '% done');
            switch (snapshot.state) {
            case firebase.storage.TaskState.PAUSED: // or 'paused'
                console.log('Upload is paused');
                break;
            case firebase.storage.TaskState.RUNNING: // or 'running'
                console.log('Upload is running');
                break;
            }
        }, function(error) {

        // A full list of error codes is available at
        // https://firebase.google.com/docs/storage/web/handle-errors
        switch (error.code) {
            case 'storage/unauthorized':
            // User doesn't have permission to access the object
            break;

            case 'storage/canceled':
            // User canceled the upload
            break;
            case 'storage/unknown':
            // Unknown error occurred, inspect error.serverResponse
            break;
        }
        }, function() {
        // Upload completed successfully, now we can get the download URL
        downloadURL = uploadTask.snapshot.downloadURL;
        console.log("download complete, url: "+downloadURL);

           // Create a reference to the file to delete
          var desertRef = storageRef.child(file.name);

          // Delete the file
          desertRef.delete().then(function() {
            // File deleted successfully
          }).catch(function(error) {
            // Uh-oh, an error occurred!
          }); 

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            
            var res = this.responseText.toString();
            console.log("response from php: "+res);
           
            //res=res.replace(/[^a-zA-Z ]/g, "");
            // if(res == "added"){
            //   Materialize.toast('UPLOAD SUCESSFULLY', 3000);
            // }else if(res=="notadded"){
            //   Materialize.toast('COULD NOT UPLOAD', 3000);
            // }
          }
        };
          
         xhttp.open("GET", "manage_add_image.php?url="+downloadURL+"&id="+<?php echo $_GET['id']; ?>, true);
        
        xhttp.send(); 
        });
        }
            }
         
                
</script>

  </body>


</html>
