<?php
session_start();
$_SESSION['admin'];
if ($_SESSION['admin'] == "") {
    header('location:index.php');
}
?>
<html>
  <head>
      <?php 
      include('../header.php');
      ?>
      <script>
        function validate(){

            var a = document.getElementById("password").value;
            var b = document.getElementById("re-password").value;
            if (a!=b) {
               alert("Passwords not match");
               return false;
            }
        }
     </script>
     
  </head>

  <body>
  <?php include('navbar.php');  
  if(isset($_GET['added']))
  {
     echo"
     <script>
     Materialize.toast('created', 5000);
     </script>
     ";
  }
  ?>
   <div class="container " style="border: 2px solid #26a69a;border-radius: 5px;padding: 2%;">
   <center><h5 style="color:#26a69a;">CREATE ALBUM</h5></center>

  <form  class = "col s12" onSubmit="return validate()" method="post" action="manage_create_album.php" enctype="multipart/form-data">
 
  <div class="row">
        <div class="input-field col s12">
          <input id="username" type="text" name="username" class="validate">
          <label for="username">Username</label>
        </div>
        </div>
  <div class="row">
        <div class="input-field col s12">
          <input id="password" type="password" name="password" class="validate">
          <label for="password">Password</label>
        </div>
        </div>
        <div class="row">
        <div class="input-field col s12">
								<input id="re-password" type="password" class="validate" name = "re-password" required>
								<label for="re-password">Retype Password</label>
							</div>
              </div>
        <div class="row">
        <div class="input-field col s12">
          <input id="album_name" type="text" name="album_name" class="validate">
          <label for="album_name">Album name</label>
        </div>
        </div>

  
      
  
  <center><button class="btn" type = "submit" name = "button" onclick="">SUBMIT</button></center>
</form>
  </div>
  </body>


</html>
