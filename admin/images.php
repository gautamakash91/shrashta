<?php
session_start();
$_SESSION['admin'];
if ($_SESSION['admin'] == "") {
    header('location:index.php');
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
  <?php 
      include_once("../db.php");
      include_once("../header.php");
      ?>
    <title>Full Screen Image Gallery </title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" type="text/css" href="css/fs-gal.css" />
    <style type="text/css">
      html, body {
        margin: 0;
        padding: 0;
        height: 100%;
        width: 100%;
        text-align: center;
        overflow:hidden;
      }
      h1, h2 {
        font-size: 300;
      }
      .fs-gal {
        height:250px;
        float: left;
       
      }
      
    </style>
  </head>
  <body>
 
  <div class="container">
<h1> Image Gallery </h1>
</div>
<div class="row"> 
 <?php
              $id=$_GET['id'];
              $querry = "select image_url from  images_album where album_id='$id'";
              $result = $conn->query($querry);
            
              while($row = $result->fetch_assoc())
               {
                ?> 
              
    <!-- Create objects with class 'fs-gal' and include the data-attribute 'data-url' with the relative or absolute path which is to be displayed. -->

    
    <img class="fs-gal col s12 m6 l3 card" src="<?php echo $row['image_url'];?>" alt=" Image " data-url="<?php echo $row['image_url'];?>"   />                
    

    <!-- Full screen gallery -->
    <div class="fs-gal-view">
      <h1></h1>
      <img class="fs-gal-prev fs-gal-nav" src="img/prev.svg" alt="Previous picture" title="Previous picture" />
      <img class="fs-gal-next fs-gal-nav" src="img/next.svg" alt="Next picture" title="Next picture" />
      <img class="fs-gal-close" src="img/close.svg" alt="Close gallery" title="Close gallery" />
    </div>
    <?php    
          }
         
          ?>
          
            </div>
        

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="js/fs-gal.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
  </body>
</html>

