<!DOCTYPE html>
<html lang = "en">
   
   <head>
   <link href="https://fonts.googleapis.com/css?family=Tangerine:400,700" rel="stylesheet">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
   <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <title>Shrashta Photography</title>
   <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

      <style>
        body  {
          background-image: url("../img/2.jpg");
          background-size:cover;
          background-repeat: no-repeat;
        }
        #navbar{
         background-color:white;
       }
       
        #navbutton{
          color:#ef486f;
          font-family:'Lora', sans-serif;
          font-size:15px;
         
          width:120px;
          text-align:center;

        }
        #navbutton:hover{
          
         background-color:#fed0db;
         opacity: 1;
         color:white;

        }
        #nav-mobile{
          margin-left:28%;
          background:white !important;
        }
        #logo{
          height:70px;
          margin-top:-8px;
        }
        #button{
          color:#ef486f;
          font-family:'Lora', sans-serif;
          font-size:20px;
        
         

        }
        .share {
  width: 42px;
  height: 144px;
  padding: 5px;
  margin: 0;
  color: #444;
  border-radius: 0 8px 8px 0;
  border: 1px solid #ccc;
  border-left-width: 0;
  box-shadow: 0px 0px 10px #ccc;
  position: fixed;
  top: calc(50% - 75px);
  left: 0px;
  z-index: 295;
  background-color:white;
}
.share.expanded {
  width: 64px;
}
.share.expanded ul.share-other, .share.expanded .share-less {
  display: block;
}
.share.expanded .share-more {
  display: none;
}
.share ul {
  padding: 0;
  margin: 0;
  display: block;
  float: left;
}
.share ul.share-other {
  display: none;
}
.share ul li {
  padding:5px;
  width: 32px;
  height: 42px;
  line-height: 32px;
  border-radius: 16px;
  font-size: 22px;
  text-align: center;
  display: block;
  list-style-type: none;
  cursor: pointer;
  transition: all .25s linear;
  z-index: 310;
}
.share ul li:hover {
  background: #aaa;
}
.share ul li a {
  color: #aaa;
}
.share ul li.share-fb:hover {
  background: #3b5999;
}
.share ul li.share-fb a {
  color: #3b5999;
}
.share ul li.share-tw:hover {
  background: red;
}
.share ul li.share-tw a {
  color: red;
}
.share ul li.share-gp:hover {
  background: #df4a32;
}
.share ul li.share-gp a {
  color: #df4a32;
}
.share ul li a:hover {
  color: white;
}

[data-tooltip] {
  position: relative;
}
[data-tooltip]:before, [data-tooltip]:after {
  visibility: hidden;
  opacity: 0;
  transition: opacity 0.2s linear, visibility 0.2s linear, transform 0.2s linear;
  transform: translate3d(0, 0, 0);
  position: absolute;
  bottom: 50%;
  left: 100%;
  pointer-events: none;
}
[data-tooltip]:before {
  content: "";
  margin: 0 0 0 -12px;
  background: transparent;
  border: 6px solid transparent;
  border-right-color: #ccc;
  top: calc(50% - 6px);
  z-index: 1001;
}
[data-tooltip]:after {
  content: attr(data-tooltip);
  height: 30px;
  line-height: 30px;
  padding: 0 8px;
  margin: 0 0 -16px 0;
  color: #444;
  background-color: #fff;
  border: 1px solid #ccc;
  border-radius: 4px;
  font-size: 14px;
  z-index: 1000;
}
[data-tooltip]:hover:before, [data-tooltip]:hover:after, [data-tooltip]:focus:before, [data-tooltip]:focus:after {
  visibility: visible;
  opacity: 1;
  transform: translateX(12px);
}

      </style>
         <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.0-beta.13/angular.min.js"></script>        
   </head>
 
    <body>
      <!-- PC NAVBAR START -->
   <div>
   <ul id="dropdown1" class="dropdown-content">
  <li><a href="../services.php#wedding"><span style="color:#ef486f;">Wedding Photography </span></a></li>
  <li class="divider"></li>
  <li><a href="../services.php#prewedding"><span style="color:#ef486f;">Pre-wedding Photography </span></a></li>
  <li class="divider"></li>
  <li><a href="../services.php#candid"><span style="color:#ef486f;">Candid Photography</span></a></li>
  <li class="divider"></li>
  <li><a href="../services.php#cinematic"><span style="color:#ef486f;">Cinematic Videography</span></a></li>
</ul>       
          
  
  <nav class="col l12 hide-on-med-and-down" id="navbar">
    <div class="nav-wrapper">
      <a href="#" class="brand-logo"><img class="responsive-img"id="logo" src="images/mainlogo1.png" alt="" style="margin-left:15px;margin-top:4px;"></a>
      <ul id="nav-mobile" class="hide-on-med-and-down">
        <li><a href="../index.php" id="navbutton">HOME</a></li>
        <li><a href="index.php" id="navbutton">E-ALBUM</a></li>
        <li><a class="dropdown-button" data-beloworigin="true" id="navbutton" href="../services.php" data-activates="dropdown1">SERVICES</a></li>
        <!-- <li><a href="" id="navbutton">Blog</a></li> -->
        <li><a href="../about us.php" id="navbutton">ABOUT US </a></li>
        <li><a href="../Contact.php" id="navbutton">CONTACT</a></li>
        

      </ul>
       
    </div>
  </nav>
   </div>
        <!-- PC NAVBAR END -->

        
    <!--____________________________________mobile________________________________________________________________________________________-->
    <div class="row show-on-medium-and-down hide-on-large-only" style="margin:0px;">
          
          <!--logo for mobile-->
          <!-- <div class="logo center white show-on-medium-and-down hide-on-large-only" id="shrastalogo">
             <img  class="responsive-img" src="images/mainlogo1.png" style="width:150px;height:70px;">
          </div>  -->
         <!--navbar for mobile-->
         <div class="col s12 black-text white" id="navbar-mobile" style="">
             <ul class="collapsible">
                 <li>
                   <div class="collapsible-header white" style="font-size:20px;"><i class="material-icons" style="position:relative;top:5px;">menu</i>Menu
                   <div class="logo center white show-on-medium-and-down hide-on-large-only" id="shrastalogo" style="position:absolute; right:40px">
                     <img  class="responsive-img" src="images/Shrashta Photography black Logo.png" style="width:70px;height:35px;">
                    </div>   
                  </div>
                   <div class="collapsible-body"><a href="../index.php" id="button"><div style="width:100%">HOME</div></a></div>
                   <div class="collapsible-body"><a href="index.php" id="button"><div style="width:100%">E-ALBUM</div></a></div>
                    <div class="collapsible-body"> <a href="../services.php" id="button"><div style="width:100%">SERVICES</div></a></div>
                   <div class="collapsible-body"><a href="../about us.php" id="button"><div style="width:100%">ABOUT US</div></a></div>
                   <div class="collapsible-body"><a href="../Contact.php" id="button"><div style="width:100%">CONTACT</div></a></div>
                 </li>
              </ul>
           </div>

       </div>
   <!--_______________________________________end of mobile header________________________________________-->
  
<aside class="share" ng-app="app" ng-controller="ShareCtrl">
  <ul>
    <li class="share-fb"><a href="https://www.facebook.com/ShrashtaCreation/?ref=br_rs" ><i class="fa fa-facebook"></i></a></li>
    <li class="share-tw"><a href="https://www.youtube.com/channel/UCvmkIuaSUbMIR3W3zZ1Edpw/videos"><i class="fa fa-youtube"></i></a></li>
    <li class="share-gp"><a href="https://plus.google.com/115064191067160860506"><i class="fa fa-google-plus"></i></a></li>
   </ul>
</aside>
        
          <div class="container center"  id="login" style="background-color:black;opacity:0.8; margin-top:50px;margin-bottom:50px;width:70%;padding-top:10px;padding-bottom:10px;" >
         
         
         
          <font color="white"> 
            <center>
              <img src="images/Shrashta Photography White Logo.png" style="width:200px; border-radius:50%;">
            </center> 
          </font>
        
          <h5 style="color:#eb658c;font-family: 'Tangerine', cursive ;font-weight:700;font-size:50px;"> Login</h5>
       
            <form class = "col s12" action = "login.php" method = "post">
            <div class="container">
                <div class="row">
                    <div class="input-field col s12">
                      <input style="color:white" type="email" id="email" class="validate" name="email" required>
                      <label style="color:white" for="email">Email :</label>
                    </div>
                </div>
                <div class="row">
                  <div class="input-field col s12">
                    <input style="color:white" type="password" id="password1" class="validate" name="password" required>
                    <label style="color:white" for="password1">Password :</label>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s6">
                      <?php   
                            if(isset($_GET['error'])){
                        ?>
							<span class="error">Wrong credentials try again!</span>
                        <?php 
                            }
                        ?>
                  </div>
                </div> 
                <input class = "white-text btn" style="background-color:#eb658c;" type = "submit" name = "login" value="Login" />
                
            </form>  
          </div> 
               
          </div>
        </div>
         
             
      <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
      
      <script type="text/javascript" src="../js/materialize.min.js"></script>
     <script>
        $(document).ready(function(){
          $('.collapsible').collapsible();
         $(".dropdown-button").dropdown({
          hover: true,
          belowOrigin: false
        }); 
        });
        </script>
   </body>


</html>

<style>
    #login{
        padding: 2%;
        border: 2px solid #eb658c;
        border-radius: 5px;
    }
    .error{
        font-size: 1.1em;
        color: white;
    }
</style>

