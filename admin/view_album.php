<?php
session_start();
$_SESSION['admin'];
if ($_SESSION['admin'] == "") {
    header('location:index.php');
}
?>
<html lang = "en">
  <head>
      <?php 
      include_once("../db.php");
      include_once("../header.php");
      if(isset($_POST['delete'])){
        $del_id = $_POST['select'];

          if($conn->query("DELETE FROM album WHERE id='$del_id'")){
            echo "
            <script>
            Materialize.toast('successfully Deleted', 5000);
            </script>
            ";
          }else{
            echo "
            <script>
            Materialize.toast('Could not be Deleted', 5000);
            </script>
            ";
          
          }
         }
      ?>
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
  </head>
    <body>
        <?php 
          include('navbar.php');
        ?>
         <div class="container">
              <h4>DELETE ALBUM</h4>
                <form method="post"  name="form" id="form" action="view_album.php"> 
                  <table id="view">
        <thead>
          <tr>
              <th>Select</th>
              <th>ID</th>
              <th>User Name</th>
              <th>Album Name</th>   
              
          </tr>
        </thead>
        
       <tbody>
       <?php
                $querry = "select * from  album";
                $result = $conn->query($querry);
                while($row = $result->fetch_assoc()) {

         ?>
      <tr>
        <td><input class="with-gap"  type="radio" id="<?php echo $row['id'];?>"  name="select"  value="<?php echo $row['id'];?>" />
        <label for="<?php echo $row['id'];?>"></label>
        </td>
        <td><?php echo $row['id'];?></td>
        <td><?php echo $row['username'];?></td>
        <td><?php echo $row['album_name'];?></td>
        
        
    </tr>
       <?php
          }
            ?>

       </tbody>
    </table>
    <br/><br/>
    <input type="submit" name="delete" value="Delete" >
</form>

<script type="text/javascript" 
    src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js">
    </script>
<script type="text/javascript" >

      $(document).ready( function () {
        $('#view').DataTable();
        } );
</script>
</div>
  </body>
  </html>