<?php
session_start();
$_SESSION['admin'];
if ($_SESSION['admin'] == "") {
    header('location:index.php');
}
?>
<html lang = "en">
  <head>
      <?php 
      include_once("../db.php");
      include_once("../header.php");
      ?>
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
  </head>
  <body>
  
<div class="container">
<h4>Album Name</h4>

 <table id="view">
 
        <thead>
          <tr>
               <th>ID</th>
              <th>Album Name</th>   
              
          </tr>
        </thead>
        
       <tbody>
       <?php
              
                $querry = "select * from  album ";
                $result = $conn->query($querry);
                while($row = $result->fetch_assoc()) {

         ?>
      <tr>
            <td><?php echo $row['id'];?></td>
        <td><a href="images.php?id=<?php echo $row['id'];?>"><?php echo $row['album_name'];?> </a></td>
        
    </tr>
    <?php
          }
            ?>
    
       </tbody>
    </table>
   
</form>

<script type="text/javascript" 
    src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js">
    </script>
<script type="text/javascript" >

      $(document).ready( function () {
        $('#view').DataTable();
        } );
</script>
</div>
  </body>
  </html>