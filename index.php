<?php
   require 'PHPMailer/PHPMailerAutoload.php';


?>
<!DOCTYPE html>
  <html>
    <head>
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--use own styling-->
         <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/css?family=Tangerine:700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <!--Let browser know website is optimized for mobile-->
      <title>Shrashta Photography</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <style>


       body{
          background:;
          height: 100%;
          width: 100%;
          margin: 0;
          padding: 0;
          
          }
          #button{
          color:#ef486f;
          font-family:'Lora', sans-serif;
          font-size:20px;
         
          }
         
      </style>
    
    
    </head>

    <body>
      <!--navbar pc or header for pc-->
      <?php  
          include ('navbar.php');
      ?>

      <!--____________________________________mobile________________________________________________________________________________________-->
        <div class="row show-on-medium-and-down hide-on-large-only" style="margin:0px;">
          
          <!--logo for mobile-->
          <!-- <div class="logo center white show-on-medium-and-down hide-on-large-only" id="shrastalogo">
             <img  class="responsive-img" src="images/mainlogo1.png" style="width:150px;height:70px;">
          </div>  -->
         <!--navbar for mobile-->
         <div class="col s12 black-text white" id="navbar-mobile" style="">
             <ul class="collapsible">
                 <li>
                   <div class="collapsible-header white" style="font-size:20px;"><i class="material-icons" style="position:relative;top:5px;">menu</i>Menu
                   <div class="logo center white show-on-medium-and-down hide-on-large-only" id="shrastalogo" style="position:absolute; right:40px">
                     <img  class="responsive-img" src="images/Shrashta Photography black Logo.png" style="width:70px;height:35px;">
                    </div>   
                  </div>
                   <div class="collapsible-body"><a href="index.php" id="button"><div style="width:100%">HOME</div></a></div>
                   <div class="collapsible-body"><a href="admin/index.php" id="button"><div style="width:100%">E-ALBUM</div></a></div>
                    <div class="collapsible-body"> <a href="services.php" id="button"><div style="width:100%">SERVICES</div></a></div>
                   <div class="collapsible-body"><a href="about us.php" id="button"><div style="width:100%">ABOUT US</div></a></div>
                   <div class="collapsible-body"><a href="Contact.php" id="button"><div style="width:100%">CONTACT</div></a></div>
                 </li>
              </ul>
           </div>

       </div>
   <!--_______________________________________end of mobile header________________________________________-->


                
                <div class="row slide" style="">
                
                      <div class="col s12 m12 l12" style="padding-left:0px;padding-top:0px;margin-top:-22px;">
                      <!-- <canvas id="canvas" style="background-color:transparent;">  -->
                      <?php 
                          include ('slider.php');
       
                         ?>   
                        <!-- </canvas> -->
                      
                      </div> 
                 </div> 
                 
                   
     


        <div class="row" style="margin-top:50px;"><!--upper body-->
                         
              <div class= " col s12 m12 l5" style="">
                  <div class="col s12 m12 l12 center">
                    <h3 style="color:#eb658c;font-family:'Tangerine', cursive;font-size:60px;">Our Story</h3>
                    <img src="images/Heart Shape.png" alt="" class="responsive-img"style="width:50%;margin-top:-10px;" >
                    <p id="body3" class="show-on-large-only hide-on-med-and-down" style="font-family:'Titillium web', sans-serif;color:black;font-size:20px;text-align:justify;margin-top:3%;margin-left:60px;">We are a  team of proffesional photographers and cinematographers dedicated to capturing the best moments of your event . We capture wedding and pre-wedding photography to leave you with amazing memories from your big day. </p>
                    <p id="body3" class="show-on-medium-and-down hide-on-large-only" style="font-family:'Titillium web', sans-serif;, sans-serif;color:black;font-size:15px;text-align:justify;margin-top:3%;">We are a  team of proffesional photographers and cinematographers dedicated to capturing the best moments of your event . We capture wedding and pre-wedding photography to leave you with amazing memories from your big day. </p>
                   
                   </div>
              </div>
              
              <div class="col  l7 hide-on-med-and-down" style="padding-top:100px;padding-left:200px;">
                  <div class="z-depth-3" style="width:400px;height:300px;background-image:url('images/textbackground1.jpg');background-size:cover;border:10px solid blanchedalmond"></div>
              </div>
              <div class="col s12 m12 show-on-medium-and-down hide-on-large-only" style="padding-top:30px;padding-left:0;padding-right:0;">
                  <div class="z-depth-3 " style="width:100%;height:300px;background-image:url('images/textbackground1.jpg');background-size:cover;border:8px solid blanchedalmond"></div>
              </div>
        </div> 
          
          <!--gallery-->
          <div class="col  l12 center hide-on-med-and-down" style="margin-top:100px;">
             <a href="ourcollections.html"> <h3 class="center-align" style=" color:#eb658c;font-family:'Tangerine', cursive;font-size:60px;" >Our Gallery </h3></a>
              <img src="images/Heart Shape.png" alt="" class="responsive-img" style="width:20%;">
          </div>
          <br>
          <div class="col s12 m12  center show-on-medium-and-down hide-on-large-only" style="margin-top:100px;">
             <a href="ourcollections.html"> <h3 class="center-align" style=" color:#eb658c;font-family:'Tangerine', cursive;font-size:60px;" >Our Gallery </h3></a>
              <img src="images/Heart Shape.png" alt="" class="responsive-img" style="width:50%;">
          </div>
          <br>
                    
           <div class="row" >
           <a href="ourcollections.html"> <div class="col s12 m6 l6 red"   style="height:500px ; background-image:url('Slider/01.jpg');background-size:cover;border:10px solid white;">
            </div></a>
            

           <a href="ourcollections.html"> <div class="col s12 m6 l3" style="height:250px ; background-image:url('Slider/07.jpg');background-size:cover;border:10px solid white;">
                
            </div></a>
            
            <a href="ourcollections.html"> <div class="col s12 m6 l3" style="height:250px ; background-image:url('image/16.jpg');background-size:cover;border:10px solid white;">
              
            </div></a>
            <a href="ourcollections.html"> <div class="col s12 l3 hide-on-med-only" style="height:250px ; background-image:url('images/03.jpg');background-size:cover;border:10px solid white;">
             
            </div></a>
            <a href="ourcollections.html"> <div class="col s12  l3 hide-on-med-only" style="height:250px ; background-image:url('images/06.jpg');background-size:cover;border:10px solid white;">
               
            </div></a>
                        
        </div> 

       
       <div class="row" style="background-color:#fdf0f0; border-top:1px #fec2d0 solid;border-bottom:1px #fec2d0 solid;margin-bottom:0px !important;">
            <div class="center col s12 m12 l12">
                  <h2 style="color:#eb658c;font-family:'Tangerine', cursive;margin-top:50px;" class="center-align hide-on-small-only">Reviews</h2>
                  <h4 style="color:#eb658c;font-family:'Tangerine', cursive;margin-top:0px;" class="center-align hide-on-med-and-up">Reviews</h4>
                  <img src="images/Heart Shape.png" alt="" class="responsive-img center-align hide-on-small-only" style="width:20%;margin-top:0px;">
                  <img src="images/Heart Shape.png" alt="" class="responsive-img center-align hide-on-med-and-up" style="width:50%;margin-top:0px;margin-bottom:20px;">
                  
                </div>
                <div class="col s12 m12 l12"style="margin-top:-30px;">
                <?php 
                     include ('slider2.php');      
                  ?>              
                </div>
       </div>
               

        <!--footer-->
               <div id="footerform" style="margin-top:-10px;">      
                <div class="row"  style="width:100%;height:750px;margin-bottom:0px;"id="contactform">
                        <h3 class="center-align white-text" style="padding-top:15px;color:white;font-family:'Tangerine', cursive;">Get In Touch</h3>
                        <p class="center-align white-text" style="color:white;font-family:'Titillium web', sans-serif;">We love hearing your story from the moment you meet to the day he proposed.Tell us what your dream wedding will look like and we will capture it.</p>
                        <form method="post" action="contact_us.php" enctype="multipart/form-data">
                        <div class="col s12 l6 m6">
                          <div class="row">
                            <div class="input-field col s12">
                              <i class="material-icons prefix"style="color:white;">account_circle</i>
                              <input id="icon_prefix" name="name" type="text" class="validate" style="color:blanchedalmond;">
                              <label for="icon_prefix"style="color:white;"> Name</label>
                            </div>
                            <div class="input-field col s12">
                              <i class="material-icons prefix" style="color:white;">location_on</i>
                              <input id="icon_telephone" name="address" type="text" class="validate" style="color:blanchedalmond;">
                              <label for="icon_telephone"style="color:white;">Address</label>
                            </div>
                            <div class="input-field col s12">
                                <i class="material-icons prefix"style="color:white;">phone</i>
                                <input id="icon_telephone" type="number" name="phone" class="validate" style="color:blanchedalmond;">
                                <label for="icon_telephone"style="color:white;">Telephone</label>
                              </div>
                              <div class="input-field col s12">
                                  <i class="material-icons prefix" style="color:white;">mail</i>
                                  <input id="icon_telephone" type="email" name="email" class="validate" style="color:blanchedalmond;">
                                  <label for="icon_telephone"style="color:white;">Email</label>
                                </div>
                          </div>
                        </div>
                        
                        <div class="col m6 l6 hide-on-small-only center-align white-text">
                              <h5>Message us</h5>
                              <textarea name="message" rows="1" cols="50" style="background-color:transparent; height:270px;color:blanchedalmond;border-color:white;"></textarea>
                              <button name="submit" class="waves-effect waves-light btn left  center" style="background-color:#ef486f;"><i class="material-icons right">send</i>Send</button>
                        </div>
                       
                        <div class="col s12 hide-on-med-and-up center-align white-text center" style="margin-top:-40px;"><!--for mbile-->
                              <h6>Message us</h6>
                              <textarea name="message" rows="1" cols="50" style="background-color:transparent; height:100px;color:blanchedalmond;border-color:white;"></textarea>
                              <button name="submit" class="waves-effect waves-light btn  " style="background-color:#ef486f;"><i class="material-icons right">send</i>Send</button>
                        </div>
                        </form>
                        <div class="col m12 l12  hide-on-small-only center-align white-text" style="position:relative;top:14%;">
                          <h6 class="center-align white-text"><span style="position:relative;top:5px;">Tel&nbsp;:</span> <span>&nbsp;+91-9583333562 <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  +91-9040250369</span></h6>
                               <div id="footer-icons" style="margin-bottom:0;">
                                <a class="waves-effect waves-light red btn-floating social google"href="https://plus.google.com/115064191067160860506"><i class="fa fa-google"></i> Sign in with Google</a>
                                <a class="waves-effect waves-light blue btn-floating social google" href=""><i class="fa fa-twitter"></i> Sign in with Google</a>
                                <a class="waves-effect waves-light red accent-4 btn-floating social google" href="https://www.youtube.com/channel/UCvmkIuaSUbMIR3W3zZ1Edpw/videos"><i class="fa fa-youtube"></i> Sign in with Google</a>
                                <a class="waves-effect waves-light  btn-floating indigo  social google" style="background-color:#3a559f;" href="https://www.facebook.com/ShrashtaCreation/?ref=br_rs"><i class="fa fa-facebook"></i> Sign in with Google</a>
                               </div>
                               <h6 class="center-align" style=" font-family: 'Titillium Web', sans-serif;font-size:15px;font-weight:400;color:white;padding-bottom:20px;margin:0px">© 2017-2018 All Rights Reserved<br>Designed By<a href="http://appstone.in" style="color:#ef789b;"> Appstone</a></h6>
                        </div>

                          <div class="col s12 center-align white-text hide-on-med-and-up" style="margin:0px"><!--for mbile-->
                          <h6 class="center-align white-text" style="padding-top:20px;"><span style="position:relative;top:5px;">Tel&nbsp;:</span> <span>&nbsp;+91-9583333562 <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  +91-9040250369</span></h6>
                               <div id="footer-icons" style="margin-bottom:0;">
                                <a class="waves-effect waves-light red btn-floating social google"href="https://plus.google.com/115064191067160860506"><i class="fa fa-google"></i> Sign in with Google</a>
                                <a class="waves-effect waves-light blue btn-floating social google" href=""><i class="fa fa-twitter"></i> Sign in with Google</a>
                                <a class="waves-effect waves-light red accent-4 btn-floating social google" href="https://www.youtube.com/channel/UCvmkIuaSUbMIR3W3zZ1Edpw/videos"><i class="fa fa-youtube"></i> Sign in with Google</a>
                                <a class="waves-effect waves-light  btn-floating indigo darken-4 social google" style="background-color:#3a559f;" href="https://www.facebook.com/ShrashtaCreation/?ref=br_rs"><i class="fa fa-facebook"></i> Sign in with Google</a>
                               </div>
                               <h6 class="center-align" style=" font-family: 'Titillium Web', sans-serif;font-size:15px;font-weight:400;color:white;padding-bottom:20px;margin:0px">© 2017-2018 All Rights Reserved<br>Designed By<a href="http://appstone.in" style="color:#ef789b;"> Appstone</a></h6>
                        </div>


                    
                     </div>

      <!--Import jQuery before materialize.js-->
    
          
      <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
      <script type="text/javascript" src="js/materialize.min.js"></script>
     <script>
         $(document).ready(function(){
        $('.carousel').carousel({indicators:true});
        $('.carousel.carousel-slider').carousel({ fullWidth: true });
        $('.slider').slider({height:580,transition:500,interval:3000,indicators:true});
        $(".dropdown-button").dropdown({
          hover: true,
          belowOrigin: false
        }); 
        });
        </script>

        <?php
      
      if(isset($_POST['submit'])) {
        $content='
        <body style="border:1px solid black;">
            <div align="center" style=" background:linear-gradient(#000000,#181818,#000000);margin:auto;width:600px" >
           <img src="https://s15.postimg.cc/gglti88cr/mainlogo1.png" style="width:200px;">
              
            </div>
            <br>
            <div style="width:400px;height:300px;margin:auto;">
                    <span style="padding-left:10px;"><b>Name:</b>'.$_POST['name'].'</span> <br><br>
                    <span style="padding-left:10px;"><b>Address:</b>'.$_POST['address'].'</span><br><br>
                    <span style="padding-left:10px;"><b>Phone No:</b>'.$_POST['phone'].'</span><br><br>
                    <span style="padding-left:10px;"><b>Email:</b>'.$_POST['email'].'</span> <br><br>
                    <span style="padding-left:10px;"><b>Message:</b>'.$_POST['message'].'</span> <br><br> 
            </div>
        </body>
        </html>
        ';
        $mail = new PHPMailer;
        $mail->isSendmail();
        //Set who the message is to be sent from
        $mail->setFrom("shrashta.creation@gmail.com","Shrashta Photography");
        //Set an alternative reply-to address
        $mail->addReplyTo($_POST['email'], $_POST['name']);
        //Set who the message is to be sent to
        $mail->addAddress('shrashta.creation@gmail.com',"Shrashta Photography"); 
        //Set the subject line
        $mail->Subject = 'From Website Contact Form';
        //Read an HTML message body from an external file, convert referenced images to embedded,
        //convert HTML into a basic plain-text alternative body
        //Replace the plain text body with one created manually
        $mail->Body    = $content;
        $mail->isHTML();
        $mail->AltBody = "";
        
        if (!$mail->send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
            echo "
            <script>
            Materialize.toast('Message could not be sent', 4000);
            <script>
            ";
        } else {
          echo "
          <script>
          Materialize.toast('Message sent successfully', 4000);
          <script>
          ";
        }
      }
      ?>
         
    </body>
  </html>
        