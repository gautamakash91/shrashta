	
      <style type="text/css">
       #navbar{
         background-color:white;
       }
       
        #navbutton{
          color:#ef486f;
          font-size:15px;
          width:120px;
         text-align:center;

        }
        #navbutton:hover{
          /* background: rgba(240,142,170,1);
background: -moz-radial-gradient(center, ellipse cover, rgba(240,142,170,1) 0%, rgba(246,246,246,1) 91%, rgba(255,255,255,1) 100%);
background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%, rgba(240,142,170,1)), color-stop(91%, rgba(246,246,246,1)), color-stop(100%, rgba(255,255,255,1)));
background: -webkit-radial-gradient(center, ellipse cover, rgba(240,142,170,1) 0%, rgba(246,246,246,1) 91%, rgba(255,255,255,1) 100%);
background: -o-radial-gradient(center, ellipse cover, rgba(240,142,170,1) 0%, rgba(246,246,246,1) 91%, rgba(255,255,255,1) 100%);
background: -ms-radial-gradient(center, ellipse cover, rgba(240,142,170,1) 0%, rgba(246,246,246,1) 91%, rgba(255,255,255,1) 100%);
background: radial-gradient(ellipse at center, rgba(240,142,170,1) 0%, rgba(246,246,246,1) 91%, rgba(255,255,255,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f08eaa', endColorstr='#ffffff', GradientType=1 ); */


       background-color:#fed0db;
         opacity: 1;
         color:white;
         


        }
        #nav-mobile{
          margin-left:28%;
          background:white !important;
        }
        #logo{
          height:70px;
          margin-top:-8px;
        }

      </style>
 
 <ul id="dropdown1" class="dropdown-content">
  <li><a href="services.php#wedding"><span style="color:#ef486f;">Wedding Photography </span></a></li>
  <li class="divider"></li>
  <li><a href="services.php#prewedding"><span style="color:#ef486f;">Pre-wedding Photography </span></a></li>
  <li class="divider"></li>
  <li><a href="services.php#candid"><span style="color:#ef486f;">Candid Photography</span></a></li>
  <li class="divider"></li>
  <li><a href="services.php#cinematic"><span style="color:#ef486f;">Cinematic Videography</span></a></li>
</ul>       
  
  <nav class="col l12 hide-on-med-and-down" id="navbar">
    <div class="nav-wrapper">
      <a href="#" class="brand-logo"><img class="responsive-img"id="logo" src="images/mainlogo1.png" alt="" style="margin-left:15px;margin-top:4px;"></a>
      <ul id="nav-mobile" class="hide-on-med-and-down">
        <li><a href="index.php" id="navbutton">HOME</a></li>
        <li><a href="admin/index.php" id="navbutton">E-ALBUM</a></li>
        <li><a class="dropdown-button" data-beloworigin="true" id="navbutton" href="services.php" data-activates="dropdown1">SERVICES</a></li>
        <!-- <li><a href="" id="navbutton">Blog</a></li> -->
        <li><a href="about us.php" id="navbutton">ABOUT US </a></li>
        <li><a href="Contact.php" id="navbutton">CONTACT</a></li>
        

      </ul>
       
    </div>
  </nav>
