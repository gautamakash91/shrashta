<!DOCTYPE html>
  <html>
    <head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/css?family=Tangerine:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400i" rel="stylesheet">
 
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet"> 
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection"/>
        <!--use own styling-->
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Shrashta Photography</title>
     <style>
       #servimg{
          position:relative;
          width:30%;
          margin:auto;
          top:-70px
       }
       @media only screen and (max-width: 600px) {
        #servimg{
          position:relative;
          width:50%;
          margin:auto;
          top:-60px
       }
        }
        #button{
          color:#ef486f;
        
          font-size:20px;
          
         
        }
       </style>
    </head>

    <body style="">
        <!-- <div class="container"> -->
          <?php 
              include('navbar.php');
          ?>       

                <!--end-->
               <!--____________________________________mobile________________________________________________________________________________________-->
        <div class="row show-on-medium-and-down hide-on-large-only" style="margin:0px;">
          
          <!--logo for mobile-->
          <!-- <div class="logo center white show-on-medium-and-down hide-on-large-only" id="shrastalogo">
             <img  class="responsive-img" src="images/mainlogo1.png" style="width:150px;height:70px;">
          </div>  -->
         <!--navbar for mobile-->
         <div class="col s12 black-text white" id="navbar-mobile" style="">
             <ul class="collapsible">
                 <li>
                   <div class="collapsible-header white" style="font-size:20px;"><i class="material-icons" style="position:relative;top:5px;">menu</i>Menu
                   <div class="logo center white show-on-medium-and-down hide-on-large-only" id="shrastalogo" style="position:absolute; right:40px">
                     <img  class="responsive-img" src="images/Shrashta Photography black Logo.png" style="width:70px;height:35px;">
                    </div>   
                  </div>
                   <div class="collapsible-body"><a href="index.php" id="button"><div style="width:100%">HOME</div></a></div>
                   <div class="collapsible-body"><a href="admin/index.php" id="button"><div style="width:100%">E-ALBUM</div></a></div>
                    <div class="collapsible-body"> <a href="services.php" id="button"><div style="width:100%">SERVICES</div></a></div>
                   <div class="collapsible-body"><a href="about us.php" id="button"><div style="width:100%">ABOUT US</div></a></div>
                   <div class="collapsible-body"><a href="Contact.php" id="button"><div style="width:100%">CONTACT</div></a></div>
                 </li>
              </ul>
           </div>

       </div>
   <!--_______________________________________end of mobile header________________________________________-->

           <div class="row" style="">
                <div class="col s12 m12 l12 center ">
                      <h3 style="color:#eb658c ;font-size:60px;font-weight:700;letter-spacing:0.3px;line-height:60px;font-family: 'Tangerine', cursive;">Our Collections</h3>
                      <img src="images/Heart Shape.png" class="responsive-img " style="width:250px;">
                </div>        
            </div>
           
    
            <!--_____________services collection_________________-->
           
           <div class="row hide-on-small-only">
                  <div class="col m6 l6" align="center" id="wedding" >
                 
                  <a href="ourcollections.html">
                    <h3 style="color:#eb658c;font-family: 'Tangerine', cursive ;font-weight:700;text-align:center;padding-top:5%;" >Wedding <br> Photography and Videography</h3>
                  </a>
                  <img src="images/Heart Shape.png" class="responsive-img " style="width:250px;">
                       <p style="text-align:center;font-family: 'Titillium Web', sans-serif;font-size:20px;font-weight:400;color:black;padding-left:50px;padding-right:30px;"> 
                       A wedding is a ceremony where two people are united in marriage for a lifetime, promising to be there for each other till death does them apart.
                            It is indeed a very special moment which needs to be preserved and we  
                            are here to do that for you! We not only capture the love that reflects from a couple 
                            but also the moments that are spent to make their dream a reality.</p>
                  </div>
                  <a href="ourcollections.html">
                  <div class="col m6 l6" style="padding:0">
                      <img src="images/1.png" style="width:100%" />
                      </a>
                  </div>
           </div>
            
            <div class="row hide-on-small-only" >
            <a href="ourcollections.html#prewedding">
            <div class="col m6 l6" style="padding:0" id="prewedding">
                      <img src="images/2.jpg" style="width:100%" />     
                      </a>
                      </div>
                      <div class="col m6 l6" align="center" id="" >
                      <a href="ourcollections.html#prewedding">
                      <h3 style="color:#eb658c;font-family: 'Tangerine', cursive ;font-weight:700;text-align:center;padding-top:5%;" >Pre-wedding <br>Photography and Videography</h3>
                      </a>
                      <img src="images/Heart Shape.png" class="responsive-img " style="width:250px;">
                       <p style="text-align:center;font-family: 'Titillium Web', sans-serif;font-size:20px;font-weight:400;color:black;padding-left:30px;padding-right:50px;">
                       Everyone wants to capture the special moments with their  partner before wedding, to cherish those moments for a lifetime! We at Shrastha do pre-wedding shoots in a personified manner. Pre-weddings are not only 
                       based upon a particular concept but also the way how it is being portrayed. </p>
                  </div>
            </div>
            <div class="row hide-on-small-only">
                  <div class="col m6 l6" align="center" id="candid">
                  <a href="ourcollections.html#candid">
                  <h3 style="color:#eb658c;font-family: 'Tangerine', cursive ;font-weight:700;text-align:center;padding-top:5%;" >Candid Photography</h3>
                </a>
                  <img src="images/Heart Shape.png" class="responsive-img " style="width:250px;">
                       <p style="text-align:center;font-family: 'Titillium Web', sans-serif;font-size:20px;font-weight:400;color:black;padding-left:50px;padding-right:30px;">
                       A good photograph keeps a moment from running away!
                        We capture each and every moment that you will treasure for life. There are pictures that capture memories and it’s not always about posing, our candid photography keeps it real and adds life to them.
                       It’s all about patience and our photographers master it.</p>
                  </div>
                  <a href="ourcollections.html#candid">
                  <div class="col m6 l6" style="padding:0">
                      <img src="images/3.png" style="width:100%" />   
                      </a>
                  </div>
           </div>
            
            <div class="row hide-on-small-only" >
            <a href="ourcollections.html#cinematic">
            <div class="col m6 l6" style="padding:0" id="cinematic">
                      <img src="images/4.png" style="width:100%" /> 
                      </a>
                      </div>
                      <div class="col m6 l6" align="center" >
                      <a href="ourcollections.html#cinematic">
                      <h3 style="color:#eb658c;font-family: 'Tangerine', cursive ;font-weight:700;text-align:center;padding-top:5%;" >Cinematic Videography</h3>
                      </a>
                      <img src="images/Heart Shape.png" class="responsive-img " style="width:250px;">
                       <p style="text-align:center;font-family: 'Titillium Web', sans-serif;font-size:20px;font-weight:400;color:black;padding-left:50px;padding-right:50px;">
                       Cinematography has the power to fortify the feeble, unify the divided, raise the abandoned and inspire the ignorant. There are pictures that you look at for a week and never think of again. Then there are pictures that you look at for a second and think of it all your life!
                        Theme and Cinematic videography is our speciality and we make sure that you get the best out of it.
                        </p>
                         </div>
            </div>
            <!-- for mob view -->
            <div class="row show-on-small hide-on-med-and-up">
                  <div class="col s12 " align="center" >
                 
                  <a href="ourcollections.html">
                    <h3 style="color:#eb658c;font-family: 'Tangerine', cursive ;font-weight:700;text-align:center;padding-top:5%;" >Wedding <br> Photography and Videography</h3>
                  </a>
                  <img src="images/Heart Shape.png" class="responsive-img " style="width:250px;">
                       <p style="text-align:center;font-family: 'Titillium Web', sans-serif;font-size:20px;font-weight:400;color:black;padding-left:50px;padding-right:30px;"> 
                       A wedding is a ceremony where two people are united in marriage for a lifetime, promising to be there for each other till death does them apart.
                            It is indeed a very special moment which needs to be preserved and we  
                            are here to do that for you! We not only capture the love that reflects from a couple 
                            but also the moments that are spent to make their dream a reality.</p>
                  </div>
                  <a href="ourcollections.html">
                  <div class="col s12 " style="padding:0">
                      <img src="images/1.png" style="width:100%" />
                      </a>
                  </div>
           </div>
            
            <div class="row show-on-small hide-on-med-and-up">
                <div class="col s12 " align="center" >
                <a href="ourcollections.html#prewedding">
                <h3 style="color:#eb658c;font-family: 'Tangerine', cursive ;font-weight:700;text-align:center;padding-top:5%;" >Pre-wedding <br>Photography and Videography</h3>
                </a>
                <img src="images/Heart Shape.png" class="responsive-img " style="width:250px;">
                  <p style="text-align:center;font-family: 'Titillium Web', sans-serif;font-size:20px;font-weight:400;color:black;padding-left:30px;padding-right:50px;">
                  Everyone wants to capture the special moments with their partner before wedding, to cherish those moments for a lifetime! We at Shrastha do pre-wedding shoots in a personified manner. Pre-weddings shoots are not only 
                  based upon a particular concept but also the way how it is being portrayed. </p>
            </div>
                  <a href="ourcollections.html#prewedding">
                  <div class="col s12 " style="padding:0">
                      <img src="images/2.jpg" style="width:100%" />     
                      </a>
                      </div>
            </div>
            <div class="row show-on-small hide-on-med-and-up">
                  <div class="col s12 " align="center" >
                  <a href="ourcollections.html#candid">
                  <h3 style="color:#eb658c;font-family: 'Tangerine', cursive ;font-weight:700;text-align:center;padding-top:5%;" >Candid Photography</h3>
                </a>
                  <img src="images/Heart Shape.png" class="responsive-img " style="width:250px;">
                       <p style="text-align:center;font-family: 'Titillium Web', sans-serif;font-size:20px;font-weight:400;color:black;padding-left:50px;padding-right:30px;">
                       A good photograph keeps a moment from running away!
                        We capture each and every moment that you will treasure for life. There are pictures that capture memories and it’s not always about posing, our candid photography keeps it real and adds life to them.
                       It’s all about patience and our photographers master it.</p>
                  </div>
                  <a href="ourcollections.html#candid">
                  <div class="col s12 " style="padding:0">
                      <img src="images/3.png" style="width:100%" />   
                      </a>
                  </div>
           </div>
            
            <div class="row show-on-small hide-on-med-and-up" >
              <div class="col s12 " align="center" >
              <a href="ourcollections.html#cinematic">
              <h3 style="color:#eb658c;font-family: 'Tangerine', cursive ;font-weight:700;text-align:center;padding-top:5%;" >Cinematic Videography</h3>
              </a>
              <img src="images/Heart Shape.png" class="responsive-img " style="width:250px;">
                <p style="text-align:center;font-family: 'Titillium Web', sans-serif;font-size:20px;font-weight:400;color:black;padding-left:50px;padding-right:50px;">
                Cinematography has the power to fortify the feeble, unify the divided, raise the abandoned and inspire the ignorant. There are pictures that you look at for a week and never think of again. Then there are pictures that you look at for a second and think of it all your life!
                Theme and Cinematic Videography is our speciality and we make sure that you get the best out of it.
                </p>
                  </div>
                  <a href="ourcollections.html#cinematic">
                  <div class="col s12 " style="padding:0">
              <img src="images/4.png" style="width:100%" /> 
              </a>
              </div>
            </div>
             <!-- End for mob view -->
           
          <!--footer-->
          <!-- <footer class="page-footer black fixed " style="padding-top:60px;"> -->
            
          <footer style="margin:0px;">
            <div class="col s12 m12 l12 black center-align white-text" style="padding-bottom:0;position:relative;top:20%;">
              <h6 class="center-align white-text" style="padding-top:20px;"><span style="position:relative;top:5px;">Tel&nbsp;:</span> <span>&nbsp;+91-9583333562 <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  +91-9040250369</span></h6>
              <div id="footer-icons" style="margin-bottom:0;margin:0px;padding-top:10px;">
              <a class="waves-effect waves-light red btn-floating social google"href="https://www.google.com/search?ei=wOHmWuy4IoG6vwTv95fIBA&q=shrashta+photography&oq=shrashta&gs_l=psy-ab.3.0.35i39k1l2j0.2401.5393.0.7286.11.11.0.0.0.0.358.1541.0j8j0j1.10.0....0...1c.1.64.psy-ab..1.10.1743.6..46j0i67k1j0i131i67k1j0i131k1j0i46k1j0i10k1.203.DludkPAiBb0"><i class="fa fa-google"></i> Sign in with Google</a>
              <a class="waves-effect waves-light blue btn-floating social google" href=""><i class="fa fa-twitter"></i> Sign in with Google</a>
              <a class="waves-effect waves-light red accent-4 btn-floating social google" href="https://www.youtube.com/channel/UCvmkIuaSUbMIR3W3zZ1Edpw/videos"><i class="fa fa-youtube"></i> Sign in with Google</a>
              <a class="waves-effect waves-light  btn-floating  social google" style="background-color:#3a559f;" href="https://www.facebook.com/ShrashtaCreation/?ref=br_rs"><i class="fa fa-facebook" ></i> Sign in with Google</a>
              </div>
              <h6 class="center-align" style=" font-family: 'Titillium Web', sans-serif;font-size:15px;font-weight:400;color:white;padding-top:10px;padding-bottom:20px;margin:0px;">© 2017-2018 All Rights Reserved<br>Designed By <a href="http://appstone.in" style="color:#ef789b;">Appstone</a></h6>
              </div>
          </footer>
                   
          <!-- </footer>          -->
    
    
    
            
      <!--Import jQuery before materialize.js-->
      
      <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
      <script type="text/javascript" src="js/materialize.min.js"></script>
     <script>
         $(document).ready(function(){
        $('.carousel').carousel({indicators:true});
        $('.carousel.carousel-slider').carousel({ fullWidth: true });
        $('.slider').slider({height:650,transition:500,interval:3000});
        $('.dropdown-button').dropdown({hover: true,belowOrigin:false});
        
        });
        
       
      
             

            
  </script>
    </body>
  </html>
        