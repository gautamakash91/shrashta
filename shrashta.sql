-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 01, 2018 at 11:10 AM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shrashta`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_login`
--

CREATE TABLE `admin_login` (
  `id` int(20) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL,
  `type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_login`
--

INSERT INTO `admin_login` (`id`, `username`, `password`, `email`, `type`) VALUES
(1, 'pragayn', 'pragayn', 'abc@gmail.com', 'super_admin'),
(1, 'pooja', 'pooja', 'das@gmail.com', 'admin'),
(5, 'priyadarshini', 'ppd', 'ppd@gmail.com', 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE `album` (
  `id` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `album_name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`id`, `username`, `password`, `album_name`) VALUES
(1, 'pragayn', 'pragyan', 'Album'),
(2, 'pooja', 'pooja', 'images'),
(3, 'appstone', 'appstone', 'photos'),
(4, 'abc', 'xyz', 'pppp'),
(5, 'ppd', 'ppd', 'sss');

-- --------------------------------------------------------

--
-- Table structure for table `images_album`
--

CREATE TABLE `images_album` (
  `album_id` int(11) NOT NULL,
  `image_url` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `images_album`
--

INSERT INTO `images_album` (`album_id`, `image_url`) VALUES
(2, 'https://firebasestorage.googleapis.com/v0/b/wedding-e5b24.appspot.com/o/images/desert1.jpeg?alt=media'),
(1, 'https://firebasestorage.googleapis.com/v0/b/wedding-e5b24.appspot.com/o/images/desert1.jpeg?alt=media'),
(1, 'https://firebasestorage.googleapis.com/v0/b/wedding-e5b24.appspot.com/o/images/desert1.jpeg?alt=media'),
(2, 'https://firebasestorage.googleapis.com/v0/b/wedding-e5b24.appspot.com/o/images/images.jpg?alt=media'),
(2, 'https://firebasestorage.googleapis.com/v0/b/wedding-e5b24.appspot.com/o/images/images.jpg?alt=media'),
(2, 'https://firebasestorage.googleapis.com/v0/b/wedding-e5b24.appspot.com/o/images/images.jpg?alt=media'),
(2, 'https://firebasestorage.googleapis.com/v0/b/wedding-e5b24.appspot.com/o/images/images.jpg?alt=media'),
(1, 'https://firebasestorage.googleapis.com/v0/b/wedding-e5b24.appspot.com/o/images/download.jpg?alt=media'),
(1, 'https://firebasestorage.googleapis.com/v0/b/wedding-e5b24.appspot.com/o/images/download.jpg?alt=media'),
(1, 'https://firebasestorage.googleapis.com/v0/b/wedding-e5b24.appspot.com/o/images/download.jpg?alt=media'),
(1, 'https://firebasestorage.googleapis.com/v0/b/wedding-e5b24.appspot.com/o/images/desert1.jpeg?alt=media'),
(1, 'https://firebasestorage.googleapis.com/v0/b/wedding-e5b24.appspot.com/o/images/desert1.jpeg?alt=media'),
(1, 'https://firebasestorage.googleapis.com/v0/b/wedding-e5b24.appspot.com/o/images/desert1.jpeg?alt=media'),
(1, 'https://firebasestorage.googleapis.com/v0/b/wedding-e5b24.appspot.com/o/images/desert1.jpeg?alt=media'),
(1, 'https://firebasestorage.googleapis.com/v0/b/wedding-e5b24.appspot.com/o/images/images.jpg?alt=media'),
(1, 'https://firebasestorage.googleapis.com/v0/b/wedding-e5b24.appspot.com/o/images/images.jpg?alt=media');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `album`
--
ALTER TABLE `album`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
